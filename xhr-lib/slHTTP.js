function slHTTP(){
    this.http = new XMLHttpRequest();
    this.state=0;
}

slHTTP.prototype.get = function(url,callback){
    this.http.open('GET',url,true);
    
    let self = this;
    this.http.onload = function(){
        if(self.http.status===200){
            callback(null,self.http.responseText);
        }
        else{
            callback('error:' +self.http.status);
        }
    }
    this.http.send();
}

slHTTP.prototype.post = function(url,data,callback){
    this.http.open('POST',url,true);
    
    let self = this;
    this.http.onload = function(){
        if(self.http.status===200||self.http.status===201){
            callback(null,self.http.responseText);
        }
        else{
            callback('error:' +self.http.status);
        }
    }
    this.http.send(JSON.stringify(data));
}
slHTTP.prototype.put = function(url,data,callback){
    this.http.open('PUT',url,true);
    
    let self = this;
    this.http.onload = function(){
        if(self.http.status===200||self.http.status===201){
            callback(null,self.http.responseText);
        }
        else{
            callback('error:' +self.http.status);
        }
    }
    this.http.send(JSON.stringify(data));
}

slHTTP.prototype.get = function(url,callback){
    this.http.open('GET',url,true);
    
    let self = this;
    this.http.onload = function(){
        if(self.http.status===200){
            callback(null,'RESOURCE DELETED.....');
        }
        else{
            callback('error:' +self.http.status);
        }
    }
    this.http.send();
}