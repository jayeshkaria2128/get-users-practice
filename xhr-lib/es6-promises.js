const students = [
    {name : 'john Doe', age:27},
    {name: 'jane Doe', age:19}
];

function createStudent(student){
    return new Promise(function(resolve,reject){
        setTimeout(function(){
            students.push(student);
            const error =false;
            if(error)
                reject('ERROR STATUS');
            else
                resolve();
        },2000);
    });
}

function getStudent(){
    let output="";
    students.forEach(function(student){
        output += `
${student.name}`;
    });
    document.body.innerHTML=output;
}

createStudent({name:'jimmy doe',age:19})
.then(function(){
    getStudent();
})
.catch(function(err){
    console.error(err);
});