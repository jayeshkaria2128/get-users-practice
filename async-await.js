//asy functin always returb promise or await can beused with async
//async function myFunction(){
//    setTimeout(() => "Hello"),1000);
//}
//
//async function myFunction(){
//    const promise = new Promise((resolve,reject) =>{
//        setTimeout(() => resolve("Hello"),2000);
//    });
//    
//    console.log(promise);
//    promise.then(data => console.log(data));
//    const res = await promise;
//    return res;
//}

//async function myFunction(){
//    const promise = new Promise((resolve,reject)) =>{
//        setTimeout(() => resolve("Hello"),2000);
//    });
//    
//    const error = true;
//    if(!error){
//        const res = await promise;
//        return res;
//    }else{
//        throw new Error("Error");
//        await Promise.reject(new Error("Something Wrong!"));
//    }
//    
//}

//myFunction()
//.then(data =>console.log(data))
//.catch(err => console.log(err));

function timePass(){
    getUsers()
    .then(data =>console.log(data))
    .catch(err => console.log(err));
    
    console.log("Following code !!");
}


async function getUsers(){
    const res = await fetch("http://jsonplaceholder.typicode.com/users");
//    console.log(res);
    const data = await res.json(); //.json b promise return krta hai !
}