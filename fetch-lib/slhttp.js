class slHTTP{
    get(url){
        return new Promise((resolve , reject)=>{
            fetch(url)
            .then(res => {
                if(!res.ok)
                    throw new Error(res.status);
                return res.json();
            })
            .then(data => resolve(data))
            .catch(err => reject(err));
        });
    }
    
    post(url,data){
        return new Promise((resolve , reject)=>{
            fetch(url,{
                method:'POST',
                headers:{
                    'content-type':'application/json'
                },
                body: JSON.stringify(data)
            })
            .then(res => {
                if(!res.ok)
                    throw new Error(res.status);
                return res.json();
            })
            .then(data => resolve(data))
            .catch(err => reject(err));
        });
    }
    
    
    put(url,data){
        return new Promise((resolve , reject)=>{
            fetch(url,{
                method:'PUT',
                headers:{
                    'content-type':'application/json'
                },
                body: JSON.stringify(data)
            })
            .then(res => {
                if(!res.ok)
                    throw new Error(res.status);
                return res.json();
            })
            .then(data => resolve(data))
            .catch(err => reject(err));
        });
    }
    
    delete(url){
        return new Promise((resolve , reject)=>{
            fetch(url,{
                method:'DELETE',
            
            })
            .then(res => {
                if(!res.ok)
                    throw new Error(res.status);
                return res.json();
            })
            .then(data => resolve('RESOURCE DELETED'))
            .catch(err => reject(err));
        });
    }
    
}