document.getElementById('button').addEventListener('click',loadData);

function loadData(){
    //console.log("hello world");
    
    //create a xhr object
    const xhr = new XMLHttpRequest();
    
    //OPEN THE CONNECTION TO REMOTE USING XHR
    /* 
    Syntax
    *open(method,url[asych,[user,[password]]]);
    */
    console.log('READY STATE :',xhr.readyState);
    
    /**
    *Ready State:
    0: request not initialized
    1: server connection established
    2:request received
    3:server processing the requeust
    4:request finished and response is  ready response mtln error b ho skta
    */
    xhr.open('GET','data.txt',true);
    //console.log('Ready state',xhr.readyState);
    xhr.onload = function(){
        console.log('Ready State in load',xhr.readyState);
        //console.log("Hello world");
        
        /** HTTP status
        200 :ok
        404:error
        401:
        */
        
        if(this.status === 200){
            document.getElementById('output').innerHTML = this.responseText;
        }
    }
    
    
    //can be used to loaders
    xhr.onprogress = function(){
        console.log(this.readyState);
    }
    
    /*xhr.onreadystatechange = function(){
        console.log(this.readyState);
        if(this.status === 200 & this.readyState == 4){
            document.getElementById('output').innerHTML = this.responseText;
        }
    }*/
    xhr.send();
}